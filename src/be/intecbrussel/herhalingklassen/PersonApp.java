package be.intecbrussel.herhalingklassen;

public class PersonApp {

	public static void main(String[] args) {

		Person alex = new Person(); // ALEX.SET ZOALS WE ONDERAAN ZIEN KAN HIER OOK TUSSEN DE HAKEN GEPLAATST WORDEN
		// (new person("Alex", 28, "male", 180, true, 5))

		alex.setName("Alex");
		alex.setAge(28);
		alex.setGender("male");
		alex.setHeight(180);
		alex.setSmoker(true);
		alex.setNumberOfHobby(20); // 20 HEEFT GEEN EFFECT, WE HEBBEN DE GROTTE VAN ARRAY AL GEINITIALISEERD IN
									// CONSTRUCTOR (5)
		alex.addHobby("doedelzakker");
		alex.addHobby("facebooker");
		alex.addHobby("omslachtigheid");
		alex.addHobby("tram reizer");
		alex.addHobby("eerste rij zitter");

		System.out.println(alex); // DIT GEEFT EEN REFERENTIEVERWIJZING

		Person copy = new Person("Mateo", 3, "unknown", 1.10, true, 20);
		copy.addHobby("wenen");
		copy.addHobby("blijten");
		copy.addHobby("zeiken");
		copy.addHobby("repeat");
		System.out.println(copy);

	}

}
