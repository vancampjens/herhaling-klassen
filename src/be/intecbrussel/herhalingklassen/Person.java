package be.intecbrussel.herhalingklassen;

import java.util.Arrays;

public class Person {

	private double height;
	private String name;
	private int age;
	private boolean smoker;
	private String gender;
	private String[] hobbies; // = new String [5]; VOOR ALS WE REEDS OPVOORHAND WETEN HOEVEEL HOBIES, ANDERS
								// GEBRUIKEN WE CODEBLOK (OF CONSTRUCTOR).
	private int numberOfHobby;

	// {
	// hobbies = new String[5];
	// }

	public Person() {

		this("", 0, "", 0, false, 5); // THIS MOET STEEDS IN DE EERSTE REGEL GEBRUIKT WORDEN. DIT ZIJN DE DEFAULT
										// WAARDEN

	}

	public Person(String name) {

		this(name, 0, "", 0, false, 5);

	}

	public Person(String name, int age) {

		this(name, age, "", 0, false, 5);

	}

	public Person(String name, int age, String gender, double height, boolean smoker, int numberOfHobby) {

		this.name = name; // ALS WE NAME EEN ANDERE NAAM GEVEN (BV NAAM) HOEFT .THIS ER NIET BIJ
		this.age = age;
		this.gender = gender;
		this.height = height;
		this.smoker = smoker;
		// hobbies = new String[5]; OMDAT WE HIER WAARDEN AANGEVEN MOETEN WE ZE NIET
		// PLAATSEN BIJ PARAMETERS
		hobbies = new String[numberOfHobby];

	}

	public Person(Person person) { // COPY CONSTRUCTOR

		this(person.getName(), person.getAge(), person.getGender(), person.getHeight(), person.isSmoker(),
				person.getNumberOfHobby());

	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isSmoker() { // MAG OOK GET.SMOKER ZIJN, MAAR BIJ BOOL WODT VAAK IS GEBRUIKT
		return smoker;
	}

	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String[] getHobbies() {
		return hobbies;
	}

	public void setHobbies(String[] hobbies) {
		this.hobbies = hobbies;
	}

	public int getNumberOfHobby() {
		return numberOfHobby;
	}

	public void setNumberOfHobby(int numberOfHobby) {
		this.numberOfHobby = numberOfHobby;
	}

	public void addHobby(String hobby) {
		String[] localArray = getHobbies();
		for (int i = 0; i < localArray.length; i++) {
			if (localArray[i] == null) { // DWZ ENKEL ALS DE PLAATS LEEG IS, MAG ER IETS INGEVULD WORDEN
				localArray[i] = hobby;
				break; // JE KAN OOK RETURN GEBRUIKEN IPV BREAK
			} else if (localArray[i] != null && i == localArray.length) {
				System.out.println("Sorry hobbies are full");
			}
		}
	}

	@Override // RECHTER MUISKNOP, SOURCE, TO STRING EN AANDUIDEN WAT JE WIL MEEGEVEN
	public String toString() {
		return "Person [height=" + height + ", name=" + name + ", age=" + age + ", smoker=" + smoker + ", gender="
				+ gender + ", hobbies=" + Arrays.toString(hobbies) + "]";
	}

}
