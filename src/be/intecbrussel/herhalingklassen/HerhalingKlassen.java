package be.intecbrussel.herhalingklassen;

public class HerhalingKlassen {

	private String name;
	String lastname;
	public String adress;

	private String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name; // HIER STAAT PERSON.NAME
	}
	

	String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

}
